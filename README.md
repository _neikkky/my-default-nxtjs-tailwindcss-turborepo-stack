# -NИЖKY~
# Next.js + Tailwind CSS + TURBOREPO - STACK


--------------
#  for macOS and Linux
rm -rf node_modules
rm -f package-lock.json
rm -f yarn.lock

# 👇️ clean npm cache
npm cache clean --force

# 👇️ install packages
npm install
--------------

# for Windows
rd /s /q "node_modules"
del package-lock.json
del -f yarn.lock

# 👇️ clean npm cache
npm cache clean --force

# 👇️ install packages
npm install
-------------
