import NavTop from "../components/NavTop";

export default function Home() {
  return(<>

  <div><NavTop/></div>
  <div>Home</div>
  <div><a href="/fontgradient">FONT GRADIENT</a></div>
  <div><a href="/fontgradient/globals">TEST FONT GRADIENT</a></div>
  <div><a href="/fontgradient/mybanka">BANKA FONT GRADIENT</a></div>
  <div><a href="/internetspeed">INTERNET SPEED</a></div>
  <div><a href="/waveclock">TIMEWAVE CLOCK</a></div>
  <div><a href="/wolframproject">WOLFRAM PROJECT</a></div>
  </>)
}