
/**
 * v0 by Vercel.
 * @see https://v0.dev/t/wwIOkG4W34I
 */
export default function Fontgradient() {
    return (
      <div className="flex flex-col space-y-8">
        <h2 className="text-4xl font-bold">
          <span className="gradient1">
            Gradient Animation 1
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient2">
            Gradient Animation 2
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient3">
            Gradient Animation 3
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient4">
            Gradient Animation 4
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient5">
            Gradient Animation 5
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient6">
            Gradient Animation 6
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient7">
            Gradient Animation 7
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient8">
            Gradient Animation 8
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient9">
            Gradient Animation 9
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient10">
            Gradient Animation 10
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient11">
            Gradient Animation 11
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient12">
            NFTs 12
          </span>
        </h2>
      </div>
    )
  }
  
  