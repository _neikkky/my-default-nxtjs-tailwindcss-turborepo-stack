export default function BankaGradient() {
    return (
      <div className="flex flex-col space-y-4 text-center">
        <h2 className="text-4xl font-bold">
          <span className="gradient1">
            Transactions
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient2">
            Digital Assets
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient3">
            NFTs
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient4">
            Expenses
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient5">
            Blockchain
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient6">
           Bank
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient7">
            Open Banking 07
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient8">
            Bitcoin SV 08
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient88">
            Bitcoin SV 88
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient9">
            MyBanka 09
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient10">
            MyBanka 10
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient11">
          MyBanka 11
          </span>
        </h2>
        <h2 className="text-4xl font-bold">
          <span className="gradient12">
            MyBanka 12
          </span>
        </h2>
      </div>
    )
  }
  
  