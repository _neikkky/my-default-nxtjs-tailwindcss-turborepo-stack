// D3.js oder Chart.js
import React from "react"
import Image from "next/image"

export default function Internetspeed() {
    const bitgiga: number = 8*1024*1024*1024
    const bytegiga: number = 1024*1024*1024
    const gigatera: number = 1024
    const gigapeta: number = 1024*1024
    const kilo: number = 1000
    const mega: number = 1000000
    const giga: number = 1000000000
    const tera: number = 1000000000000
    const datavolume: number = 1.92
    // const datavolume: number = 12500

    // Berechnung der Download-Zeiten in Sekunden
    const downloadTime56Kbit = datavolume * bitgiga / (56 * kilo);
    const downloadTime1Mbit = datavolume * bitgiga / (1 * mega);
    const downloadTime16Mbit = datavolume * bitgiga / (16 * mega);
    const downloadTime25Mbit = datavolume * bitgiga / (25 * mega);
    const downloadTime50Mbit = datavolume * bitgiga / (50 * mega);
    const downloadTime100Mbit = datavolume * bitgiga / (100 * mega);
    const downloadTime250Mbit = datavolume * bitgiga / (250 * mega);
    const downloadTime1000Mbit = datavolume * bitgiga / (1 * giga);
    const downloadTime2000Mbit = datavolume * bitgiga / (2 * giga);
    const downloadTime10Gbit = datavolume * bitgiga / (10 * giga);
    const downloadTime25Gbit = datavolume * bitgiga / (25 * giga);
    const downloadTime40Gbit = datavolume * bitgiga / (40 * giga);
    const downloadTime60Gbit = datavolume * bitgiga / (60 * giga);
    const downloadTime1Tbit = datavolume * bitgiga / (1 * tera);
    const downloadTime319Tbit = datavolume * bitgiga / (319 * tera);

    const bytesTotal = datavolume * (bytegiga)
    const SatoshiPerBytes = 0.01
    const BSVprice = 50

    console.log("Sekunden: " + downloadTime1000Mbit)

    // Umwandlung der Download-Zeiten in Stunden, Minuten und Sekunden
    const formatTime = (timeInSeconds: number) => {
        const days = Math.floor(timeInSeconds / 86400);
        const hours = Math.floor((timeInSeconds % 86400) / 3600);
        const minutes = Math.floor((timeInSeconds % 3600) / 60);
        const seconds = Math.floor(timeInSeconds % 60);

        let formattedTime = "";
        if (days > 0) {
            formattedTime += `${days}d `;
        }
        if (hours > 0 || days > 0) {
            formattedTime += `${hours}h `;
        }
        if (minutes > 0 || hours > 0 || days > 0) {
            formattedTime += `${minutes}min `;
        }
        { formattedTime += ` ${seconds}s`;}
        
        return formattedTime;
    };
    return (
        <>
            <div className="m-0 p-0 w-full h-full text-center align-middle justify-center">
                    {/* <Image src="/content/971388081f6601b0e502adbfceef68d152e7f27ba5aff0230d2567aaa8acb768_0" width={36} height={36} alt="BSV Inscription" title="BSV Inscription"></Image> */}
                    <Image src="/favicon.ico" width={36} height={36} alt="BSV Inscription" title="BSV Inscription"/>
            </div>
            <h1 className="center">The data volume is {datavolume} GB</h1>
            <h1 className="center">or {Math.round(bytesTotal)} Bytes or {datavolume/gigatera} Terabytes</h1>
            {/* <h1 className="center">or {datavolume/gigapeta} Petabytes</h1> */}
            <br></br>
            <h1 className="center">Satoshi per bytes are: {SatoshiPerBytes} Satoshi / Byte</h1>
            <h1 className="center">Which means: Uploading this datavolume costs: {Math.round(bytesTotal*SatoshiPerBytes)} Satoshi</h1>
            <h1 className="center">Which are: {(Math.round(bytesTotal*SatoshiPerBytes))*0.00000001} Bitcoin</h1>
            
            <br></br>
            <h1 className="center">Price per BSV is 50€</h1>
            <h1 className="center">So this 1SatOrdinal Upload will cost: {((BSVprice*bytesTotal*SatoshiPerBytes*0.00000001).toFixed(2))} Euro</h1>
            
            <br></br>
            <div id="myBar56"
                className="h-[1.6rem] whitespace-nowrap rounded font-bold text-center text-white bar56"
                style={{     
                    width: '100%',
                    backgroundColor: "#4CAF50",
                    animation: `expand ${downloadTime56Kbit}s linear`,
                }}>
                <p>56 Kbit/s: {formatTime(downloadTime56Kbit)}</p>
            </div>
            <div id="myBar1"
                className="h-[1.6rem] whitespace-nowrap w-[0%] rounded font-bold text-center text-white"
                style={{
                    width: '100%',
                    backgroundColor: "#4CAF50",
                    animation: `expand ${downloadTime1Mbit}s linear`,
                }}>
                <p>1 Mbit/s: {formatTime(downloadTime1Mbit)}</p>
            </div>
            <div id="myBar16"
            className="h-[1.6rem] whitespace-nowrap w-[0%] rounded font-bold text-center text-white"
            style={{
                width: '100%',
                backgroundColor: "#4CAF50",
                animation: `expand ${downloadTime16Mbit}s linear`,
            }}
            >
                <p>16 Mbit/s: {formatTime(downloadTime16Mbit)}</p>
            </div>
            <div id="myBar25"
            className="h-[1.6rem] whitespace-nowrap w-[0%] rounded font-bold text-center text-white"
            style={{
                width: '100%',
                backgroundColor: "#4CAF50",
                animation: `expand ${downloadTime25Mbit}s linear`,
            }}
            >
                <p>25 Mbit/s: {formatTime(downloadTime25Mbit)}</p>
            </div>
            <div id="myBar50"
            className="h-[1.6rem] whitespace-nowrap w-[0%] rounded font-bold text-center text-white"
            style={{
                width: '100%',
                backgroundColor: "#4CAF50",
                animation: `expand ${downloadTime50Mbit}s linear`,
            }}
            >
            <p>50 Mbit/s: {formatTime(downloadTime50Mbit)}</p>
            </div>
    
            <div id="myBar100"
                className="h-[1.6rem] whitespace-nowrap w-[0%] rounded font-bold text-center text-white"
                style={{
                    width: '100%',
                    backgroundColor: "#4CAF50",
                    animation: `expand ${downloadTime100Mbit}s linear`,
                }}
                ><span>100 Mbit/s: {formatTime(downloadTime100Mbit)}</span>
            </div>
            <div id="myBar250"
                className="h-[1.6rem] whitespace-nowrap w-[0%] rounded font-bold text-center text-white"
                style={{
                    backgroundColor: "#4CAF50",
                    width: "100%",
                    animation: `expand ${downloadTime250Mbit}s linear`,
                }}
                ><span className="whitespace-nowrap">250 Mbit/s: {formatTime(downloadTime250Mbit)}</span>
            </div>
            <div id="myBar1000"
                className="h-[1.6rem] whitespace-nowrap w-[0%] rounded font-bold text-center text-white "
                style={{
                    backgroundColor: "#4CAF50",
                    width: "100%",
                    animation: `expand ${downloadTime1000Mbit}s linear`
                }}
                > <p className="whitespace-nowrap">1000 Mbit/s - cat5e & cat6 Ethernet: {formatTime(downloadTime1000Mbit)}</p>
            </div>
            <div id="myBar1000"
                className="h-[1.6rem] whitespace-nowrap w-[0%] rounded font-bold text-center text-white "
                style={{
                    backgroundColor: "#4CAF50",
                    width: "100%",
                    animation: `expand ${downloadTime2000Mbit}s linear`
                }}
                > <p className="whitespace-nowrap">2000 Mbit/s - cat5e & cat6 Ethernet: {formatTime(downloadTime2000Mbit)}</p>
            </div>
            <div id="myBar1000"
                className="h-[1.6rem] whitespace-nowrap w-[0%] rounded font-bold text-center text-white "
                style={{
                    backgroundColor: "#4CAF50",
                    width: "100%",
                    animation: `expand ${downloadTime10Gbit}s linear`
                }}
                > <p className="whitespace-nowrap">10 Gbit/s - cat6a & cat7 Ethernet: {formatTime(downloadTime10Gbit)}</p>
            </div>
            <div id="myBar1000"
                className="h-[1.6rem] whitespace-nowrap w-[0%] rounded font-bold text-center text-white "
                style={{
                    backgroundColor: "#4CAF50",
                    width: "100%",
                    animation: `expand ${downloadTime25Gbit}s linear`
                }}
                > <p className="whitespace-nowrap">25 Gbit/s - cat7 & cat8.1 Ethernet Cable: {formatTime(downloadTime25Gbit)}</p>
            </div>
            <div id="myBar1000"
                className="h-[1.6rem] whitespace-nowrap w-[0%] rounded font-bold text-center text-white "
                style={{
                    backgroundColor: "#4CAF50",
                    width: "100%",
                    animation: `expand ${downloadTime40Gbit}s linear`
                }}
                > <p className="whitespace-nowrap">40 Gbit/s - cat8.2 Ethernet Cable: {formatTime(downloadTime40Gbit)}</p>
            </div>
            <div id="myBar1000"
                className="h-[1.6rem] whitespace-nowrap w-[0%] rounded font-bold text-center text-white "
                style={{
                    backgroundColor: "#4CAF50",
                    width: "100%",
                    animation: `expand ${downloadTime60Gbit}s linear`
                }}
                > <p className="whitespace-nowrap">60 Gbit/s - Samsung 990 EVO M2.SSD: {formatTime(downloadTime60Gbit)}</p>
            </div>
            <div id="myBar1T"
                className="h-[1.6rem] whitespace-nowrap rounded font-bold text-center text-white bar1T bg-[#222] ease-linear duration-[${downloadTime1Tbit}s]"
                style={{
                    backgroundColor: "#222",
                    width: "100%",
                    animation: `expand ${downloadTime1Tbit}s linear`
                }}>
                <div className="text1T">1 Tbit/s: {formatTime(downloadTime1Tbit)}</div>
            </div>
            <div id="myBar1T"
                className="h-[1.6rem] whitespace-nowrap rounded font-bold text-center text-white bar1T bg-[#222] ease-linear duration-[${downloadTime1Tbit}s]"
                style={{
                    backgroundColor: "#222",
                    width: "100%",
                    animation: `expand ${downloadTime319Tbit}s linear`
                }}>
                <div className="text1T">World fastest speed: 319 Tbit/s: {formatTime(downloadTime319Tbit)}</div>
            </div>
    </>
    );
}
