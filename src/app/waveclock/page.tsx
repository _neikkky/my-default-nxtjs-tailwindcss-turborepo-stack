interface WaveProps {
    sunrise: number;
    highestSunrise: number;
    sunset: number;
    midnight: number;
}

const WaveClock: React.FC<WaveProps> = ({sunrise, sunset}) => {

    const numPoints = 60*60*24
    let path = ""
    for (let i = 0; i < numPoints; i++) {
      const x = i
      const y = 75 * (1 + Math.sin((x / 300 * 2 * Math.PI + Math.PI)))
      path += (i === 0 ? "M" : "L") + x + "," + y
    }
  
    return (
    <>
        <div className="headeranimationlinear">
            <svg id="sinewave">
            <path id="myPath" fill="none" stroke="white" strokeWidth="7" d={path} />
            </svg>
        </div>
        <div className="be4afta">Hello</div>
    </>
    )
}

export default WaveClock;

