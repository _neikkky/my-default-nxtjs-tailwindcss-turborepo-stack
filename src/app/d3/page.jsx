import * as d3 from 'd3'

var svg = d3.select("body")
  .append("svg")
  .attr("width", 500)
  .attr("height", 500);

// Skalen definieren
var xScale = d3.scaleLinear()
  .domain([0, 2 * Math.PI])
  .range([0, 500]);

var yScale = d3.scaleLinear()
  .domain([-1, 1])
  .range([500, 0]);

// Daten für die Sinuswelle generieren
var data = d3.range(0, 2 * Math.PI, 0.01).map(function(t) {
  return {x: t, y: Math.sin(t)};
});

// Linie zeichnen
var line = d3.line()
  .x(function(d) { return xScale(d.x); })
  .y(function(d) { return yScale(d.y); });

svg.append("path")
  .datum(data)
  .attr("d", line)
  .attr("stroke", "black")
  .attr("fill", "none");
