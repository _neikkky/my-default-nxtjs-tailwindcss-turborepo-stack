export default function Button({
    children,
  }: {
    children: React.ReactNode
  }) {
    return (
    <>
    <div>
        {children}
    </div>
    </>
)}