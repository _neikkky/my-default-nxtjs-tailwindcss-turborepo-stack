/** @type {import('tailwindcss').Config} */
module.exports = {
  // prefix: 'tw-',
  content: [
    './app/**/*.{js,ts,jsx,tsx,mdx}', // Note the addition of the `app` directory.
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
 
    // Or if using `src` directory:
    './src/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  darkMode: 'class',
  daisyui: {
    themes: [
      {mytheme:{
      "light-mode": "#FFFFFF",
      "dark-mode": "000000",
      "primary": "#000000",
      "secondary": "#343232",
      "accent": "#343232",
      "neutral": "#272626",
      "base-100": "#000000",
      "info": "#0000FF",
      "success": "#008000",
      "warning": "#FFFF00",
      "error": "#FF0000",
    }}],
    styled: true,
    base: true,
    utils: true,
    logs: true,
    rtl: false,
    prefix: "",
    darkTheme: "black",
  },
  theme: {
    screens: {
      'sm': '640px',
      // => @media (min-width: 640px) { ... }

      'md': '768px',
      // => @media (min-width: 768px) { ... }

      'lg': '1080px',
      // => @media (min-width: 1024px) { ... }

      'xl': '1280px',
      // => @media (min-width: 1280px) { ... }

      '2xl': '1536px',
      // => @media (min-width: 1536px) { ... }
      '4xl': '2160px',
    },
    fontFamily: {
      monoton: ['Monoton','cursive'],
      sans: ['Graphik', 'sans-serif'],
      serif: ['Merriweather', 'serif'],
    },
    extend: {
      colors: {
        moderatorange: '#eb6e52',
        epromgreen: '#7ead02',
        youtube:  '#b10000',
        twitter:  '#1d9bf0',
        twetch:   '#252630',
        neutral:{
          200: '#e4e4e7',
          300: '#d4d4d4',
          400: '#a3a3a3',
          500: '#737373',
          800: '#262626',
          900: '#171717',
        },
        brown: {
          50: '#fdf8f6',
          100: '#f2e8e5',
          200: '#eaddd7',
          300: '#e0cec7',
          400: '#d2bab0',
          500: '#bfa094',
          600: '#a18072',
          700: '#977669',
          800: '#846358',
          900: '#43302b',
        },
        'trueyellow': '#FFFF00',

      },
      keyframes: {
          'fade-in-down': {
              '0%': {
                  opacity: '0',
                  transform: 'translateY(-10px)'
              },
              '100%': {
                  opacity: '1',
                  transform: 'translateY(0)'
              },
          },
      },
      animation: {
        'fade-in-down': 'fade-in-down 0.5s ease-out',
        'gradient-x': 'gradient-x 3s ease infinite',
      },
    
      spacing: {
        '128': '32rem',
        '144': '36rem',
      },
      borderRadius: {
        '4xl': '2rem',
      }
    }
  },
  plugins: [
    require("daisyui"),
    require('@headlessui/tailwindcss'),
  ],
  future: {},
  variants: {},
}

